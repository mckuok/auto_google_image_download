from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait


class webpage_is_ready(object):

    def __init__(self):
        pass

    def __call__(self, driver):
        try:
            state = driver.execute_script('return document.readyState;')
            if state == 'complete':
                return True
        except Exception:
            pass

        return False

def auto_search(keywords, count):
    '''
    keywords: a list of keywords to google image search for
    count: an integer specifying the number of images nneeds to be saved PER keyword
    
    return: a list of urls for the images scraped from the google image search result of the keywords. 
            len(urls) == len(keywords) * count
    '''
    if not isinstance(keywords, list):
        keywords = [keywords]
    
    count = int(count)
    total_urls = []    
    driver = webdriver.Chrome('chromedriver.exe')
    for keyword in keywords:
        driver.get("https://www.google.com/search?tbm=isch&q={}".format(keyword))
        WebDriverWait(driver, 10).until(webpage_is_ready())
        
        urls = []
        while len(urls) < count:
            urls += driver.execute_script("return ([]).slice.call(document.querySelectorAll('.rg_di .rg_meta')).map(image => JSON.parse(image.innerText).ou);")
            driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        
        total_urls += urls[: count]

    driver.close()
    return total_urls

if __name__ == '__main__':
    results = auto_search(['apple', 'orange'], 2500)
    print(len(results))
